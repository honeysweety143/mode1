package pack1;
/*
 * Write a program to assign the current thread to t1. 
 * Change the name of the thread to MyThread. Display the changed name of the thread.
 *  Also it should display the current time. Put the thread to sleep for 10 seconds 
 *  and display the time again remove the try{}catch(){} block surrounding the sleep
 *   method and try to execute the code. What is your observation? 
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class Question2 {

			public static void main(String[] args) throws InterruptedException {
				Question1 t1 = new Question1();

				System.out.println("Name of t1:" + t1.getName());

				t1.start();

				t1.setName("MyThread");
				System.out.println("After changing name of t1:" + t1.getName());
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
				System.out.println("time : " + sdf.format(date));

				Thread.sleep(10000);
				Date date1 = new Date();
				SimpleDateFormat sdf1 = new SimpleDateFormat("kk:mm:ss");
				System.out.println("time after sleep : " + sdf1.format(date1));

			}

		

}


