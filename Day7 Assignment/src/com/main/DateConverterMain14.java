package com.main;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import com.model.UserCode14;

public class DateConverterMain14 {

	public static void main(String[] args) throws ParseException {
		Scanner sc = new Scanner(System.in);
	    System.out.println("enter your date (dd/mm/yyyy) : ");
		String str = sc.next();
		Date date = UserCode14.convertDateFormate(str);
		System.out.println("date in the format : dd-mm-yyyy ");
		System.out.println(new SimpleDateFormat(" dd-mm-yyyy ").format(date));
		
		
	}
		

	}


