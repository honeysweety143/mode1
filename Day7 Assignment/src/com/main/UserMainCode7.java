package com.main;
 import java.util.Scanner;
 public class UserMainCode7 {
 public static int areCornerEqual(String s)
 {
	 int n=s.length();
	 if(n<2)
		 return-1;
	 if(s.charAt(0)==s.charAt(n-1))
		 return 1;
	 else 
		 return 0;
 }


	public static void main(String[] args) {
		Scanner sc= new  Scanner(System.in);
		System.out.println("enter string");
		String s = sc.next();
		int res = areCornerEqual(s);
		sc.close();
		if(res==-1)
			System.out.println("invalid input");
		else if(res==1) 
			System.out.println("valid");
		else 
			System.out.println("not valid");

	}

}
