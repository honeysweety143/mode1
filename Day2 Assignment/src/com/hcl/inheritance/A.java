package com.hcl.inheritance;

public class A {
	int studNo;
	String studName;
	public int getStudNo() {
		return studNo;
	}
	public void setStudNo(int studNo) {
		this.studNo = studNo;
	}
	public String getStudName() {
		return studName;
	}
	public void setStudName(String studName) {
		this.studName = studName;
	}
	public void display() {
		System.out.println("Display in parent student no " + this.getStudNo());
		System.out.println("Display in parent student name " +this.getStudName());
		
     
}

}
