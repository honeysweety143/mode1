package com.hcl.inheritance;

class Animal{
	void walk() {
		System.out.println("I am walking");
	}
}
class Dog extends Animal{
	void eat() {
		System.out.println("I am eating");
	}
}class Bark extends Dog{
	void bar(){
		System.out.println("I am barking");
	}
}
public class InheritanceAnimalMain {

	public static void main(String[] args) {
		Bark bark =new Bark();
		bark.walk();
		bark.eat();
		bark.bar();

	}

}
