package com.hcl.inheritance;


public class InheritanceMain1 {

	public static void main(String[] args) {
		A student=new A();
		student.setStudNo(123);
		student.setStudName("deepu");
		student.display();
		
		B student1=new B();
		student1.setStudNo(456);
		student1.setStudName("STUDENT");
	    student1.setBranch("ECE");
		student1.display();
		
		A obj =new B();
		obj.setStudNo(45);
		obj.display();
	


	}

}
