/**Program to Swap two numbers
 * 
 * @author Deepthi Ragamsetti
 *
 */
public class Swap {

	public static void main(String[] args) {
		int num1=2,num2=4;
		num1=num1+num2;
		num2=num1-num2;
		num1=num1-num2;
		System.out.println("num1 is " +num1);
		System.out.println("num2 is " +num2);

	}

}
