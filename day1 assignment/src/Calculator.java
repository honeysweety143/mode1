/**program to implement calculation. 
 * 
 * @author Deepthi Ragamsetti
 *
 */
public class Calculator {

	public static void main(String[] args) {
		int num1 = 20;
		int num2 = 4;
		int sum = num1 + num2;
		int diff = num1 - num2;
		int multiplication = num1 * num2;
		float division = num1 / num2;
		float remainder = num1 % num2;
		System.out.println("sum is " + sum);
		System.out.println("difference is " + diff);
		System.out.println("multiplication is " + multiplication);
		System.out.println("division is " + division);
		System.out.println("remainder is " + remainder);

	}

}
