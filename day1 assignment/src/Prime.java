/**Program to find whether the input is prime or not.
 * 
 * @author Deepthi Ragamsetti
 *
 */

public class Prime {

	public static void main(String[] args) {
		int num = 23,i;
		boolean flag=false;
		for (i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0) {
				System.out.println("given number is not prime");
				flag=true;
				break;
			}
		}
		if(flag==false) {
			System.out.println("given number is prime");
		}
	}

}
