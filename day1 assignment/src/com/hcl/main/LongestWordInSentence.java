/**Program to find longest word in sentence.
 *
 * @author Deepthi Ragamsetti
 *
 */
 package com.hcl.main;
public class LongestWordInSentence {

	public static void main(String[] args) {
		String s="good evening to all";
		String[] word = s.split(" ");
		String longWord = "";
		for(int i=0;i<word.length;i++) {
				if (word[i].length()>=longWord.length()) {
					 longWord=word[i];
					 
				}
		}
		System.out.println("longest word in sentence is " + longWord);
				
	}
		


}
