
/**Program to find sum of odd digits in number.
 * 
 * @author Deepthi Ragamsetti
 *
 */
package com.hcl.main;
public class SumOfDigitsInNumberMain {

	public static void main(String[] args) {
		SumOfOddDigitsInNumber check = new SumOfOddDigitsInNumber();
		if (check.checkSum(3651) == 1) {
			System.out.println("Sum of odd digits is odd");
		} else

		{
			System.out.println("Sum of odd digits is even");
		}

	}

}
