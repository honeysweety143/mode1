/**program to find avarage of two inputs.
 * 
 * @author Deepthi Ragamsetti
 *
 */
public class Average {

	public static void main(String[] args) {
		int num1 = 2, num2 = 4;
		int average = (num1 + num2) / 2;
		System.out.println(average);

	}

}
