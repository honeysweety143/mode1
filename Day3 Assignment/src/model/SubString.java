package model;

public class SubString { 
	public String str;
	public String substr;
	public SubString() {
		super();
	}
	public SubString(String str) {
		super();
		this.str = str;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	
	public String getSubstr() {
		return substr;
	}
	public void setSubstr(String substr) {
		this.substr = substr;
	}
	public void extractSubStr(int fromIndex,int toIndex) {
		substr = str.substring( fromIndex, toIndex);	
	}
      public void display() {
    	  System.out.println("the string " + str);
    	  System.out.println("the substring " + substr);
      }
	
}
