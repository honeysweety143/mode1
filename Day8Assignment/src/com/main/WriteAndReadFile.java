package com.main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriteAndReadFile {

	public static void main(String[] args) {
		String data = "Data to be written on a file";
		try {
			FileWriter output = new FileWriter("C:\\javafile\\abc.txt");
			output.write(data);
			System.out.println("success");
			output.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	
	//public class ReadFromFile{
		
	char [] array=new char[100];//static memory allocation
    try {
		FileReader input =new FileReader("C:\\javafile\\abc.txt");
		input.read(array);
		System.out.println(array);
		
	} catch (FileNotFoundException e) {
		
		e.printStackTrace();
	}catch ( IOException e) {
		e.printStackTrace();
	}
}

	}


