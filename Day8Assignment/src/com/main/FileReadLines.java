package com.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileReadLines {

	public static void main(String[] args) throws IOException {
		String fileName = "C:\\javafile\\abc.txt";
		String line = null;
		
			BufferedReader bw =new BufferedReader(new FileReader(fileName));
			int i=0;
			try {
				while(((line=bw.readLine()) != null) && i<5) {
					System.out.println(line);
					i++;
					
				}
	 
		}
    finally {
    	bw.close();
    }
	}

}
