package multipleExceptionMain;

import java.util.Scanner;

import multipleException.CustomException;
import multipleException.MyException;

public class CustomExceptionMain {

	public static void main(String[] args) {
		System.out.println("enter player name");
		Scanner scanner = new Scanner(System.in);
		String str =scanner.next();
		System.out.println("enter age");
		Scanner scanner1 = new Scanner(System.in);
	
		int a=scanner1.nextInt();
		CustomException custom=new CustomException();
		boolean val=false;
		try {
			System.out.println("player name :" +str);
			System.out.println("player age : " +a);
			val=custom.validateAge(a);
			
		}
		catch(MyException e) {
			System.out.println(e);
		}

	}

}
