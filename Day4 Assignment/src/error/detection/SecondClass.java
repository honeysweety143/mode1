package error.detection;

public class SecondClass {
	double marks2;

	public SecondClass(double marks2) {
		super();
		this.marks2 = marks2;
	}

	public double getMarks2() {
		return marks2;
	}

	public void setMarks2(double marks2) {
		this.marks2 = marks2;
		System.out.println("marks2 = " +marks2);
	}

}
